import Dexie from 'dexie';

interface IFriend {
    id?: number;
    name?: string;
    age?: number;
}

class FriendDatabase extends Dexie {
    friends: Dexie.Table<IFriend,number>;

    constructor() {
        super("FriendsDatabase");
        this.version(1).stores({
            friends: "++id,name,age"
        });
    }
}

var db = new FriendDatabase();
db.friends.add({name: "Vũ Tiến Hải", age: 27}).then(()=>{
    return db.friends.where("age").below(30).toArray();
}).then(friend => {
    console.log(friend);
    alert ("Friend: " + JSON.stringify(friend));
}).catch(e => {
    alert("error: " + e.stack || e);
});
